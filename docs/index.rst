Pybencoding Documentation
=========================

.. toctree::
   :hidden:

   pybencoding

What
----
pybencoding is a pure python 3 implementation of the `bittorrent bencoding
serialization scheme`__. The goal of this library is to encode and decode small
pieces of data, so performance is not specifically considered.

__ https://wiki.theory.org/BitTorrentSpecification#Bencoding

Why
---
The library was created because the author had issues getting several of the
'bencoding' results in pip to work and if `XKCD 927`__ has taught us anything it's
that we should create another implementation.

__ https://xkcd.com/927/
   
Quickstart
----------
::

    import pybencoding

    obj = ['hello bencoding', 42, {'key': 'value'}]
    enc = pybencoding.encode(obj)
    print('encoded the object to:', enc)
    dec = pybencoding.decode(enc)
    print('decoded the object to:', dec)

