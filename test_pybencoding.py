import pytest
from pybencoding import (_consume, _decode_int, _decode_str, _decode_list,
                         _decode_dict, _decode, decode, encode, EncodeError,
                         DecodeError)

def test_consume_one():
    assert _consume(b'abc', b'ab') == (b'a', b'bc')
def test_consume_two():
    assert _consume(b'abc', b'ab', many=True) == (b'ab', b'c')
def test_consume_three():
    with pytest.raises(DecodeError):
        _consume(b'abc', b'b')
def test_consume_four():
    assert _consume(b'abc', b'b', optional=True) == (b'', b'abc')


def test_decode_int_one():
    assert _decode_int(b'i-013e') == (-13, b'')
def test_decode_int_two():
    assert _decode_int(b'i-0e') == (0, b'')
def test_decode_int_three():
    with pytest.raises(DecodeError):
        _decode_int(b'i-e') == (0, b'')
def test_decode_int_four():
    assert _decode_int(b'i999999999999e') == (999999999999, b'')
def test_decode_int_five():
    with pytest.raises(DecodeError):
        _decode_int(b'')
def test_decode_int_six():
    with pytest.raises(DecodeError):
        _decode_int(b'i0')
def test_decode_int_seven():
    with pytest.raises(DecodeError):
        _decode_int(b'nope')

def test_decode_str_one():
    assert _decode_str(b'0:nope') == (b'', b'nope')
def test_decode_str_two():
    with pytest.raises(DecodeError):
        assert _decode_str(b'nope:nope')
def test_decode_str_three():
    assert _decode_str(b'04:nope') == (b'nope', b'')
def test_decode_str_four():
    with pytest.raises(DecodeError):
        _decode_str(b'7:nope')


def test_decode_list_one():
    assert _decode_list(b'le') == ([], b'')
def test_decode_list_two():
    with pytest.raises(DecodeError):
        assert _decode_list(b'li32e')
def test_decode_list_three():
    _decode_list(b'llei1e5:helloe') == ([[], 1, b'hello'], b'')


def test_decode_dict():
    assert (_decode_dict(b'd5:hello5:worldi32ei33ee') ==
            ({b'hello': b'world', 32: 33}, b''))


def test_decode_one():
    with pytest.raises(DecodeError):
        decode(b'i32e trailing stuff')
def test_decode_two():
    assert decode(b'd5:helloi32ee') == {b'hello': 32}
def test_decode_three():
    with pytest.raises(DecodeError):
        decode(b'l ')



def test_encode_one():
    assert encode({}) == b'de'
def test_encode_two():
    assert (encode({'hello': b'world', b'world': 'hi'}) ==
            b'd5:hello5:world5:world2:hie')
def test_encode_three():
    assert encode([]) == b'le'
def test_encode_four():
    assert encode([[], []]) == b'llelee'
def test_encode_five():
    assert encode(['hello', b'world', [32, 33]]) == b'l5:hello5:worldli32ei33eee'
def test_encode_six():
    with pytest.raises(EncodeError):
        encode({33: 34})
def test_encode_seven():
    with pytest.raises(EncodeError):
        encode(set())
def test_encode_eight():
    with pytest.raises(EncodeError):
        encode({b'hello': 'world', 'hello': 'world'})
