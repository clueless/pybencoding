pybencoding is a pure python 3 implementation of the bittorrent bencoding
serialization scheme. The goal of this library is to encode and decode small
pieces of data, so performance is not specifically considered.

The library was created because the author had issues getting several of the
'bencoding' results in pip to work and if XKCD 927 has taught us anything it's
that we should create another implementation.


  "Be conservative in what you do, be liberal in what you accept from others."
      -- Jon Postel

Some examples of that in this library:
    - Decode invalid integers such as b'i-0e' and b'i0003e' but  never generate
      those when encoding integers.
    - Decode dictionaries with keys other than byte strings, but it will never
      generate those when encode dicts.
    - Decode invalid byte strings such as '005:hello' but never generate those
      when encoding strings.
